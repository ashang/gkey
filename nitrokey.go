package main

import "bytes"
import "encoding/binary"
import "strconv"
import "crypto/rand"

type NitrokeyPro struct {
	Hid
	pwUser, pwAdmin struct {
		data [25]byte
		set  bool
	}
}

func (n *NitrokeyPro) Info() (fw [2]byte, sn uint32, otpConf [2]byte, err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_STATUS
	err = n.Transaction(&c, &r)
	bytes := bytes.NewBuffer(r.Payload[:])
	binary.Read(bytes, binary.LittleEndian, &fw)
	binary.Read(bytes, binary.LittleEndian, &sn)
	binary.Read(bytes, binary.LittleEndian, &[3]byte{})
	binary.Read(bytes, binary.LittleEndian, &otpConf)
	return
}

func RndBytes(data []byte) (err error) {
	_, err = rand.Read(data)
	return
}

func (n *NitrokeyPro) FirstAuthenticate(pw []byte) (err error) {
	err = RndBytes(n.pwAdmin.data[:])
	if err != nil {
		return
	}
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = FIRST_AUTHENTICATE
	copy(c.Payload[0:25], pw[:])
	copy(c.Payload[25:50], n.pwAdmin.data[:])
	err = n.Transaction(&c, &r)
	n.pwAdmin.set = err == nil
	return err
}

func (n *NitrokeyPro) AuthenticateUser(pw []byte) (err error) {
	err = RndBytes(n.pwAdmin.data[:])
	if err != nil {
		return
	}
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = USER_AUTHENTICATE
	copy(c.Payload[0:25], pw[:])
	copy(c.Payload[25:50], n.pwUser.data[:])
	err = n.Transaction(&c, &r)
	n.pwUser.set = err == nil
	return
}

func (n *NitrokeyPro) AuthorizeUser(crc [4]byte) error {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = USER_AUTHORIZE
	copy(c.Payload[0:4], crc[:])
	copy(c.Payload[4:29], n.pwUser.data[:])
	err := n.Transaction(&c, &r)
	n.pwUser.set = false //Workaround for firmware bug. TODO: Check version
	return err
}

func (n *NitrokeyPro) GetPinRetryCounts() (user, admin uint8, err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_USER_PASSWORD_RETRY_COUNT
	err = n.Transaction(&c, &r)
	user = r.Payload[0]
	if err != nil {
		return
	}
	c.CommandId = GET_PASSWORD_RETRY_COUNT
	err = n.Transaction(&c, &r)
	admin = r.Payload[0]
	return
}

func (n *NitrokeyPro) UnlockPws(pw []byte) (err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = PW_SAFE_ENABLE
	copy(c.Payload[0:30], pw[:])
	err = n.Transaction(&c, &r)
	return
}

func (n *NitrokeyPro) GetPwsSlotStatus() (slots [PWS_SLOT_COUNT]uint8, err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_PW_SAFE_SLOT_STATUS
	err = n.Transaction(&c, &r)
	copy(slots[:], r.Payload[:])
	return
}

func (n *NitrokeyPro) GetPwsSlotList() (names []string, ids []uint8, _ error) {
	status, err := n.GetPwsSlotStatus()
	if err != nil {
		return names, ids, err
	}
	//Count used slots
	slots := 0
	for _, enabled := range status {
		if enabled == 1 {
			slots++
		}
	}
	names = make([]string, 0, slots)
	ids = make([]uint8, 0, slots)
	for slot, enabled := range status {
		if enabled == 1 {
			ids = append(ids, uint8(slot))
			byt, err := n.GetPwsSlotName(uint8(slot))
			names = append(names, string(bytes.TrimRight(byt[:], "\x00")))
			if err != nil {
				return names, ids, err
			}
		}
	}
	return names, ids, nil
}

func (n *NitrokeyPro) GetPwsSlotName(slot uint8) (name [PWS_SLOTNAME_LENGTH]uint8, err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_PW_SAFE_SLOT_NAME
	c.Payload[0] = slot
	err = n.Transaction(&c, &r)
	copy(name[:], r.Payload[:])
	return
}

func (n *NitrokeyPro) GetPwsSlotSecret(slot uint8) (secret [PWS_PASSWORD_LENGTH]uint8, err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_PW_SAFE_SLOT_PASSWORD
	c.Payload[0] = slot
	err = n.Transaction(&c, &r)
	copy(secret[:], r.Payload[:])
	return
}

func (n *NitrokeyPro) GetPwsSlotLogin(slot uint8) (login [PWS_LOGINNAME_LENGTH]uint8, err error) {
	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_PW_SAFE_SLOT_LOGINNAME
	c.Payload[0] = slot
	err = n.Transaction(&c, &r)
	copy(login[:], r.Payload[:])
	return
}

func (n *NitrokeyPro) GetPwsSlotData(slot uint8) (data PwsSlot, _ error) {
	if byt, err := n.GetPwsSlotName(slot); err == nil {
		data.Name = string(bytes.TrimRight(byt[:], "\x00"))
	} else {
		return data, err
	}
	if byt, err := n.GetPwsSlotLogin(slot); err == nil {
		data.Login = string(bytes.TrimRight(byt[:], "\x00"))
	} else {
		return data, err
	}
	if byt, err := n.GetPwsSlotSecret(slot); err == nil {
		data.Secret = string(bytes.TrimRight(byt[:], "\x00"))
	} else {
		return data, err
	}
	return data, nil
}

type PwsState []PwsSlot
type PwsSlot struct {
	Name, Login, Secret string
}

func (n *NitrokeyPro) ExportPws() (pws PwsState, _ error) {
	status, err := n.GetPwsSlotStatus()
	if err != nil {
		return pws, err
	}
	//Count used slots
	slots := 0
	for _, enabled := range status {
		if enabled == 1 {

			slots++
		}
	}
	pws = make([]PwsSlot, 0, slots)
	for slot, enabled := range status {
		if enabled == 1 {
			data, err := n.GetPwsSlotData(uint8(slot))
			pws = append(pws, data)
			if err != nil {
				return pws, err
			}
		}
	}
	return pws, nil
}

type OtpState struct {
	Totp []OtpSlot `json:",omitempty"`
	Hotp []OtpSlot `json:",omitempty"`
}
type OtpSlot struct {
	Name    string
	Config  uint8
	TokenId string
	Counter uint64
	Secret  [20]byte `json:"-"`
}

func (n *NitrokeyPro) ExportOtp() (otp OtpState, _ error) {
	otp.Totp = make([]OtpSlot, 0, 16)
	otp.Hotp = make([]OtpSlot, 0, 16)
	for slot := uint8(0); slot < 16; slot++ {
		data, err := n.GetTotpSlotData(slot)
		if err == ErrOtpEmptySlot {
			continue
		}
		if err == ErrOtpInvalidSlotId {
			break
		}
		otp.Totp = append(otp.Totp, data)
		if err != nil {
			return otp, err
		}
	}
	for slot := uint8(0); slot < 16; slot++ {
		data, err := n.GetHotpSlotData(slot)
		if err == ErrOtpEmptySlot {
			continue
		}
		if err == ErrOtpInvalidSlotId {
			break
		}
		otp.Totp = append(otp.Hotp, data)
		if err != nil {
			return otp, err
		}
	}
	return
}

func (e OtpError) Error() (s string) {
	s = "{OtpError: "
	switch e {
	case ErrOtpInvalidSlotId:
		s += "Invalid slot id"
	case ErrOtpEmptySlot:
		s += "Empty slot"
	default:
		s += strconv.FormatUint(uint64(e), 16)
	}
	s += "}"
	return
}

type OtpError uint32

const (
	ErrOtpInvalidSlotId OtpError = 0
	ErrOtpEmptySlot     OtpError = 1
)

func (n *NitrokeyPro) GetTotp(slot uint8) (uint32, error) {
	if slot&0xF0 != 0 {
		return 0, ErrOtpInvalidSlotId
	}
	slot += 32
	return n.GetOtp(slot)
}

func (n *NitrokeyPro) GetOtp(slot uint8) (otp uint32, err error) {
	//Workaround for bug in Nitrokey Pro 1.0 firmware
	if slot == 0x13 || slot == 0x2F { //TODO: Check version
		return 0, ErrOtpInvalidSlotId
	}

	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = GET_CODE
	c.Payload[0] = slot
	err = n.AuthorizeUser(c.GetCRC())
	if err != nil {
		return
	}
	err = n.Transaction(&c, &r)
	otp = binary.LittleEndian.Uint32(r.Payload[0:4])
	return
}

func (n *NitrokeyPro) GetTotpSlotList() (names []string, ids []uint8, _ error) {
	names = make([]string, 0, 16)
	ids = make([]uint8, 0, 16)
	for slot := uint8(0); slot < 16; slot++ {
		data, err := n.GetTotpSlotData(uint8(slot))
		if err == ErrOtpInvalidSlotId {
			break
		}
		if err == ErrOtpEmptySlot {
			continue
		}
		names = append(names, data.Name)
		ids = append(ids, slot)
		if err != nil {
			return names, ids, err
		}
	}
	return
}

func (n *NitrokeyPro) GetOtpSlotData(slot uint8) (data OtpSlot, _ error) {
	//Workaround for bug in Nitrokey Pro 1.0 firmware
	if slot == 0x13 || slot == 0x2F { //TODO: Check version
		return data, ErrOtpInvalidSlotId
	}

	var c CommandReport
	var r ResponseReport
	defer r.Zero()
	c.CommandId = READ_SLOT
	c.Payload[0] = slot
	err := n.Transaction(&c, &r)
	{
		var name [OTP_SLOTNAME_LENGTH]byte
		var tokenId [OTP_TOKEN_ID_LENGTH]byte
		byteReader := bytes.NewBuffer(r.Payload[:])
		binary.Read(byteReader, binary.LittleEndian, &name)
		data.Name = string(bytes.TrimRight(name[:], "\x00"))
		binary.Read(byteReader, binary.LittleEndian, &data.Config)
		binary.Read(byteReader, binary.LittleEndian, &tokenId)
		data.TokenId = string(bytes.TrimRight(tokenId[:], "\x00"))
		binary.Read(byteReader, binary.LittleEndian, &data.Counter)
	}
	//Deescalate expected errors to OtpError
	switch err {
	case ErrCmdOtpEmptySlot:
		err = ErrOtpEmptySlot
	case ErrCmdOtpInvalidSlotId:
		err = ErrOtpInvalidSlotId
	}
	return data, err
}

func (n *NitrokeyPro) GetHotpSlotData(slot uint8) (data OtpSlot, _ error) {
	if slot&0xF0 != 0 {
		return data, ErrOtpInvalidSlotId
	}
	slot += 16
	return n.GetOtpSlotData(slot)
}

func (n *NitrokeyPro) GetTotpSlotData(slot uint8) (data OtpSlot, _ error) {
	if slot&0xF0 != 0 {
		return data, ErrOtpInvalidSlotId
	}
	slot += 32
	return n.GetOtpSlotData(slot)
}

type State struct {
	PwsState `json:"Pws,omitempty"`
	OtpState `json:",omitempty"`
}

func (n *NitrokeyPro) Export() (state State, err error) {
	state.PwsState, err = n.ExportPws()
	if err != nil {
		return
	}
	state.OtpState, err = n.ExportOtp()
	return
}
