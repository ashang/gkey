# Gkey #

Command line tool for [NitrokeyPro](https://www.nitrokey.com).

Project is on hold for now. What it does it does well, but that is all it will do, unless you fork it. I have a couple of pointers for simplification if anyone actually wants to continue.

### Features ###

* Retrieve passwords (by slot name).
* Generate TOTP (by slot name).
* Export TOTP, HOTP and password safe slots as JSON object (does not include OTP secrets, which can not be read).

### Usage ###

This assumes the binary (gkey) is executable and in a directory that is listed in the PATH environment variable. Otherwise change to the directory that contains the binary (e.g. cd ~/Downloads/), make the binary executable (chmod +x gkey) and use ./gkey instead of gkey.

```
gkey ACTION [SLOTTYPE [SLOTNAME]]
  ACTION:
    list [SLOTTYPE] - List slotnames
    export - Print all slot information (JSON)
    get SLOTTYPE SLOTNAME - Get/generate password
  SLOTTYPE:
    totp - Perform action on TOTP slot(s)
    pws - Perform action on password safe slot(s)
  SLOTNAME:
    (string) - Name of slot
```

### Download ###

Linux binaries are provided on the [Downloads page](https://bitbucket.org/MaVo159/gkey/downloads).

### Build ###

Requires a go compiler (golang-go) and hidapi (libhidapi-dev).

* git clone https://bitbucket.org/MaVo159/gkey.git
* cd gkey
* go build

### Planned features (in order of importance) ###

* Configuring slots
* Import from JSON (including OTP secrets)
* Split into command  line tool and library
* C bindings for library
* ...
* OS X and Windows compatibility (help needed)

### Feedback ###

* Please use the issue tracker for bug reports, feature requests and other feedback.